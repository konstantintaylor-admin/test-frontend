export default function Banner() {
  return (
    <div className="relative bg-[#213F4E]">
      <div className="mx-auto max-w-7xl py-2 px-3 text-center sm:px-6 lg:px-8">
        <div className="sm:px-16 sm:text-center">
          <p className="font-medium text-white">
            <div className="text-sm sm:text-lg md:inline">
              <span className=" mr-2 text-yellow-500">FREE SHIPPING</span>
              <span>ON ALL ORDERS!</span>
            </div>
          </p>
        </div>
      </div>
    </div>
  );
}
