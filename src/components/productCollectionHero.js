/* This example requires Tailwind CSS v2.0+ */
export default function ProductCollectionHero() {
  return (
    <section className=" bg-indigo-200 sm:mt-2">
      <div className="relative flex h-48 sm:h-60 justify-center lg:block w-full flex-col lg:h-full">
        <div className="absolute inset-0">
          <img
            className="h-full w-full object-cover object-left"
            src="https://cdn.builder.io/api/v1/image/assets%2F08d8ed1927034a9791a787768e108aa6%2Fdf3a0c00d9d44b3984ad5f2937ed97c1"
            alt=""
          />
          <div
            className="absolute inset-0 mix-blend-multiply"
            aria-hidden="true"
          />
        </div>
        <div className="max-w-9xl relative my-auto sm:mr-auto py-24 px-4 sm:py-32 sm:px-6 lg:px-20">
          <h1 className="text-center text-4xl font-medium text-white placeholder:tracking-tight sm:text-left sm:text-5xl lg:text-6xl">
            Make 2022 Their <br /> Best Year Yet
          </h1>
        </div>
      </div>
      <div className="w-full bg-[#bc2d3e] py-1 text-center text-xl sm:text-3xl font-light italic text-white ">
        25% Off Our Best-Sellers + Free Gift
      </div>
    </section>
  );
}
