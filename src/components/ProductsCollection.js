// COMPONENTS
import ProductCollectionHero from "./productCollectionHero";
import Sidebar from "./Sidebar";
import Warning from "../components/Warning";
import Pagination2 from "../components/Pagination2";

// HOOKS & LIBRARIES
import { useEffect, useState } from "react";
import { useProvider } from "../contexts/ProductsProvider";
import axios from "axios";

import { Fade } from "@stahl.luke/react-reveal";

export default function ProductsCollection() {
  const {
    data,
    setData,
    currentPage,
    setCurrentPage,
    pageLimit,
    searchValue,
    setSearchValue,
    priceSearchValue,
    setPriceSearchValue,
    catOrDog,
    subscription,
  } = useProvider();

  const [filterOrSortValue, setFilterOrSortValue] = useState("");
  const [operation, setOperation] = useState("");
  const [sortValue, setSortValue] = useState("");

  useEffect(() => {
    getAllProducts();
  }, []);

  const getAllProducts = async () => {
    await axios.get("http://localhost:3010/products").then((res) => {
      setData(res.data);
    });
  };
  // ADD QUERY TO GET REQUEST WHEN SUB IS TRUE
  const subTrue = () => {
    return `&subscription=${subscription}`;
  };

  // PAGINATION
  const indexOfLastProduct = currentPage * pageLimit;
  const indexOfFirstProduct = indexOfLastProduct - pageLimit;
  const currentProducts = data.slice(indexOfFirstProduct, indexOfLastProduct);
  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  // GET PRODUCTS AFTER SEARCH OR FILTER
  const getProducts = async (start, end, optType = null) => {
    switch (optType) {
      case "searchTitle":
        setOperation(optType);
        setSearchValue("");
        return await axios
          .get(
            `http://localhost:3010/products?title_like=${searchValue}&_start=${start}&_end=${end}`
          )
          .then((res) => {
            setData(res.data);
            setCurrentPage(1);
          })
          .catch((err) => {
            console.log(err.message);
          });
      case "searchPrice":
        setOperation(optType);
        setPriceSearchValue("");
        return await axios
          .get(
            `http://localhost:3010/products?_sort=price&_order=desc&price_lte=${priceSearchValue}&_start=${start}&_end=${end}`
          )
          .then((res) => {
            setData(res.data);
            setCurrentPage(1);
          })
          .catch((err) => {
            console.log(err.message);
          });

      case "filter":
        setOperation(optType);
        setFilterOrSortValue(filterOrSortValue);
        return await axios
          .get(
            `http://localhost:3010/products?tags_like=${catOrDog}${
              subscription ? subTrue() : ""
            }&_start=${start}&_end=${end}`
          )
          .then((res) => {
            setData(res.data);
            setCurrentPage(1);
          })
          .catch((err) => {
            console.log(err.message);
          });
      default:
        return await axios
          .get(`http://localhost:3010/products?_start=${start}&_end=${end}`)
          .then((res) => {
            setData(res.data);
            setCurrentPage(1);
          })
          .catch((err) => {
            console.log(err.message);
          });
    }
  };

  return (
    <section className="bg-white">
      <div>
        <div className=" mx-auto w-full sm:max-w-[62rem] sm:px-4 lg:max-w-[110rem] lg:px-8">
          <ProductCollectionHero />

          <div className=" px-5 pt-12 pb-24 sm:px-0 lg:grid lg:grid-cols-4 lg:gap-x-8 xl:grid-cols-5">
            <Sidebar
              getProducts={getProducts}
              getAllProducts={getAllProducts}
              currentProducts={currentProducts}
            />

            <section
              aria-labelledby="product-heading"
              className="productsContainer mt-6 lg:col-span-3 lg:mt-0 xl:col-span-4 shadow-md p-2"
            >
              <h2 id="product-heading" className="sr-only">
                Products
              </h2>
              <p className="mr-auto hidden text-gray-400 lg:block">
                {currentProducts.length} results on this page
              </p>
              <div className="relative grid grid-cols-2 gap-x-5 gap-y-4 sm:grid-cols-3 sm:gap-x-6 sm:gap-y-10 lg:gap-x-0 xl:grid-cols-4">
                {currentProducts.length > 0 ? (
                  currentProducts.map(
                    ({
                      id,
                      title,
                      price,
                      tags,
                      subscription,
                      option_value,
                      url,
                      image_src,
                    }) => (
                      <Fade cascade key={id}>
                        <div className="group relative flex cursor-pointer flex-col overflow-hidden rounded-[0.250rem] bg-white p-2  transition duration-300 ease-linear  hover:shadow-xl">
                          <div className="overflow-hidden bg-gray-50 py-5 shadow-sm transition duration-300 ease-linear group-hover:opacity-75">
                            <img
                              src={image_src}
                              alt={tags[(0, 1)]}
                              className="h-full w-full  transform object-cover object-center transition duration-[400ms] ease-linear group-hover:scale-[1.2] sm:h-full sm:w-full"
                            />
                          </div>
                          <div className="flex flex-1 flex-col space-y-2 px-0 pt-4">
                            <h3 className="text-base font-medium text-[#001c72]">
                              <a href={url}>
                                <span
                                  aria-hidden="true"
                                  className="absolute inset-0"
                                />
                                {title}
                              </a>
                            </h3>
                            <div className="flex flex-1 flex-col justify-end">
                              <p className="text-sm font-normal text-[#001c72]">
                                {option_value}
                              </p>
                              <div className="flex space-x-1">
                                {tags.map((tag) => (
                                  <p
                                    key={tag}
                                    className="text-xs font-normal text-gray-400 opacity-100"
                                  >
                                    {tag}
                                  </p>
                                ))}
                              </div>
                              <p className="text-sm font-light text-gray-400 ">
                                subscription:{" "}
                                {subscription ? "Available" : "Not Available"}
                              </p>
                              <p className="mt-5 text-sm font-light text-[#bc2d3e] opacity-80">
                                From £{price}
                              </p>
                            </div>
                            <button className="w-full rounded-[0.250rem] bg-[#bc2d3e] py-3 font-light tracking-widest text-white">
                              SHOP NOW
                            </button>
                          </div>
                        </div>
                      </Fade>
                    )
                  )
                ) : (
                  <Warning />
                )}
              </div>

              <Pagination2
                pageLimit={pageLimit}
                totalProducts={data.length}
                paginate={paginate}
                indexOfFirstProduct={indexOfFirstProduct}
                indexOfLastProduct={indexOfLastProduct}
              />
            </section>
          </div>
        </div>
      </div>
    </section>
  );
}
