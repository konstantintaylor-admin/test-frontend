/* This example requires Tailwind CSS v2.0+ */
import {
  ArrowNarrowLeftIcon,
  ArrowNarrowRightIcon,
} from "@heroicons/react/solid";

export default function Pagination({
  // getProducts,
  // operation,
  totalProducts,
  pageLimit,
  paginate,
  indexOfFirstProduct,
  indexOfLastProduct,
}) {
  const pageNumber = [];

  for (let i = 1; i <= Math.ceil(totalProducts / pageLimit); i++) {
    pageNumber.push(i);
  }

  return (
    <nav className="my-4 mx- flex place-items-center justify-center border-t border-gray-200 px-6 sm:px-0">
      <div>
        <div className="flex md:-mt-px ">
          {pageNumber.map((number) => (
            <button
              onClick={() => paginate(number)}
              key={number}
              className="inline-flex items-center focus:border-t-2 border-indigo-500 px-4 pt-4 text-md sm:text-xl font-medium text-indigo-600 "
              aria-current="page"
            >
              {number}
            </button>
          ))}
        </div>
      </div>
    </nav>
  );
}
