/* This example requires Tailwind CSS v2.0+ */
import { XCircleIcon } from "@heroicons/react/solid";

export default function Warning() {
  return (
    <div className="absolute top-2 w-full rounded-md bg-blue-500 p-4">
      <div className="flex">
        <div className="flex-shrink-0">
          <XCircleIcon className="h-5 w-5 text-white" aria-hidden="true" />
        </div>
        <div className="ml-3">
          <h3 className="text-sm font-medium text-white">
            Oops! Looks like there were no matches for that. Please search for
            another product.
          </h3>
        </div>
      </div>
    </div>
  );
}
