import { Fragment, useEffect, useState } from "react";
import axios from "axios";
import { Dialog, Disclosure, Transition } from "@headlessui/react";
import {
  AdjustmentsIcon,
  CurrencyPoundIcon,
  SearchIcon,
  XIcon,
} from "@heroicons/react/outline";
import { ChevronDownIcon } from "@heroicons/react/solid";
import { useProvider } from "../contexts/ProductsProvider";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function Sidebar({
  getProducts,
  getAllProducts,
  currentProducts,
}) {
  // MOBILE MENU STATE
  const [mobileFiltersOpen, setMobileFiltersOpen] = useState(false);

  // CONTEXT STATE
  const {
    data,
    setData,
    searchValue,
    setSearchValue,
    priceSearchValue,
    setPriceSearchValue,
    catOrDog,
    setCatOrDog,
    subscription,
    pageLimit,
    setSubscription,
    currentPage,
  } = useProvider();

  const indexOfLastProduct = currentPage * pageLimit;
  const indexOfFirstProduct = indexOfLastProduct - pageLimit;

  // FILTERS NAMES
  const filters = [
    {
      id: "typeofpet",
      name: "Type Of Pet",
      options: [
        {
          value: "Dog",
          label: "Dog",
          image_src:
            "https://cdn.builder.io/api/v1/image/assets%2F08d8ed1927034a9791a787768e108aa6%2F5af4be288dbd4295933e39b5d13a9ecb",
        },
        {
          value: "Cat",
          label: "Cat",
          image_src:
            "https://cdn.builder.io/api/v1/image/assets%2F08d8ed1927034a9791a787768e108aa6%2Fb9f52d440ff547e585b37935bef9d044",
        },
      ],
    },
    {
      id: "subscription",
      name: "Subscription",
      options: [
        { value: "true", label: "Yes" },
        { value: "false", label: "No" },
      ],
    },
  ];

  // SEARCH, FILTER FUNCTIONS
  const handleSearch = async (e) => {
    e.preventDefault();
    getProducts(0, indexOfLastProduct, "searchTitle");
  };
  const handlePriceSearch = async (e) => {
    e.preventDefault();
    getProducts(0, indexOfLastProduct, "searchPrice");
  };

  const handleFilter = async () => {
    getProducts(0, indexOfLastProduct, "filter");
  };

  const handleReset = () => {
    getAllProducts();
    setSubscription(null);
    setCatOrDog("");
  };

  const resetSub = () => {
    setSubscription("");
  };

  useEffect(() => {
    if (catOrDog !== "") {
      handleFilter();
    }
  }, [catOrDog, subscription]);

  return (
    <aside className="col-span-1">
      {/* Mobile filter dialog */}
      <Transition.Root show={mobileFiltersOpen} as={Fragment}>
        <Dialog
          as="div"
          className="fixed inset-0 z-40 flex lg:hidden"
          onClose={setMobileFiltersOpen}
        >
          <Transition.Child
            as={Fragment}
            enter="transition-opacity ease-linear duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="transition-opacity ease-linear duration-300"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>

          <Transition.Child
            as={Fragment}
            enter="transition ease-in-out duration-300 transform"
            enterFrom="translate-x-full"
            enterTo="translate-x-0"
            leave="transition ease-in-out duration-300 transform"
            leaveFrom="translate-x-0"
            leaveTo="translate-x-full"
          >
            {/* MOBILE FILTER MENU */}
            <div className="relative  ml-auto flex h-full w-full max-w-xs flex-col overflow-y-auto bg-white py-4 pb-6 shadow-xl">
              <div className="flex items-center justify-between px-4">
                <h2 className="text-lg font-medium text-gray-900">Filters</h2>
                <button
                  type="button"
                  className="-mr-2 flex h-10 w-10 items-center justify-center p-2 text-gray-400 hover:text-gray-500"
                  onClick={() => setMobileFiltersOpen(false)}
                >
                  <span className="sr-only">Close menu</span>
                  <XIcon className="h-6 w-6" aria-hidden="true" />
                </button>
              </div>

              {/* MOBILE SEARCH */}
              <form
                onSubmit={handleSearch}
                className="mt-8 min-w-0 md:px-8  lg:px-0 xl:col-span-6"
              >
                <h2 className="mx-4 text-[#001c72]">Search for a product</h2>
                <div className="mx-4 flex items-center py-4 md:mx-auto md:max-w-3xl lg:mx-0 lg:max-w-none xl:px-0">
                  <div className="w-full">
                    <label htmlFor="search" className="sr-only">
                      Search
                    </label>
                    <div className="relative">
                      <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                        <SearchIcon
                          className="h-5 w-5 text-gray-400 group-focus:animate-pulse "
                          aria-hidden="true"
                        />
                      </div>
                      <input
                        id="search"
                        name="search"
                        value={searchValue}
                        onChange={(e) => setSearchValue(e.target.value)}
                        className="block w-full rounded-md border border-gray-300 bg-white py-2 pl-10 pr-3 text-sm placeholder-gray-500 focus:border-indigo-500 focus:text-gray-900 focus:placeholder-gray-400 focus:outline-none focus:ring-1 focus:ring-indigo-500 sm:text-sm"
                        placeholder="Search"
                        type="search"
                      />
                    </div>
                  </div>
                </div>
              </form>
              <form onSubmit={handlePriceSearch}>
                <div className="min-w-0 mx-4  flex-1 md:px-8 lg:px-0 xl:col-span-6">
                  <h2 className="mt-4  text-sm font-medium text-[#001c72]">
                    Search By Price
                  </h2>
                  <div className="flex items-center px-0 py-4 md:mx-auto md:max-w-3xl lg:mx-0 lg:max-w-none xl:px-0">
                    <div className="w-full">
                      <label htmlFor="search" className="sr-only">
                        Search
                      </label>
                      <div className="relative">
                        <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                          <CurrencyPoundIcon
                            className="h-5 w-5 text-gray-400"
                            aria-hidden="true"
                          />
                        </div>
                        <input
                          id="search"
                          name="search"
                          value={priceSearchValue}
                          onChange={(e) => setPriceSearchValue(e.target.value)}
                          className="block w-full rounded-md border border-gray-300 bg-white py-2 pl-10 pr-3 text-sm placeholder-gray-500 focus:text-gray-900 focus:placeholder-gray-400 focus:shadow-xl focus:outline-none focus:ring-1  sm:text-sm"
                          placeholder="Search"
                          type="search"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </form>
              {/* Filters */}
              <div className="mt-2">
                {filters.map((category) => (
                  <Disclosure
                    as="div"
                    key={category.name}
                    className="border-t border-gray-200 pt-4 pb-4"
                  >
                    {({ open }) => (
                      <fieldset>
                        <legend className="w-full px-2">
                          <Disclosure.Button className="flex w-full items-center justify-between p-2 text-gray-400 hover:text-gray-500">
                            <span className="text-sm font-medium text-gray-900">
                              {category.name}
                            </span>
                            <span className="ml-6 flex h-7 items-center">
                              <ChevronDownIcon
                                className={classNames(
                                  open ? "-rotate-180" : "rotate-0",
                                  "h-5 w-5 transform"
                                )}
                                aria-hidden="true"
                              />
                            </span>
                          </Disclosure.Button>
                        </legend>
                        <Disclosure.Panel className="px-4 pt-4 pb-2">
                          <div
                            className={`space-y-6 ${
                              category.name === "Type Of Pet"
                                ? "flex justify-between"
                                : null
                            }`}
                          >
                            {category.options.map(
                              ({ id, value, label, image_src }, optionIdx) =>
                                image_src ? (
                                  <button
                                    onClick={() => setCatOrDog(() => value)}
                                    key={image_src}
                                    className="flex items-center"
                                  >
                                    <div
                                      className={`h-28 w-28 transform overflow-hidden rounded-full bg-[#fcf1e0] object-cover object-top `}
                                    >
                                      <img
                                        className={`transition duration-300 ease-linear ${
                                          catOrDog === "" && "translate-y-10"
                                        } ${
                                          catOrDog === "Dog" && label === "Cat"
                                            ? "translate-y-10 hover:translate-y-0"
                                            : "translate-y-0 hover:translate-y-0"
                                        } ${
                                          catOrDog === "Cat" && label === "Dog"
                                            ? "translate-y-10 hover:translate-y-0"
                                            : "translate-y-0 hover:translate-y-0"
                                        }`}
                                        src={image_src}
                                        alt=""
                                      />
                                    </div>
                                  </button>
                                ) : (
                                  <button
                                    key={id}
                                    onClick={() => setSubscription(() => value)}
                                    type="button"
                                    className="block w-full cursor-pointer place-items-center justify-center rounded border border-gray-300 bg-white px-2.5 py-1.5 text-xs font-medium text-gray-700 shadow-sm transition duration-200 ease-linear hover:bg-gray-50 hover:shadow-md focus:bg-gray-50 focus:shadow-lg focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                                  >
                                    <label className="cursor-pointer text-center text-sm  text-gray-600">
                                      {label}
                                    </label>
                                  </button>
                                )
                            )}
                          </div>
                        </Disclosure.Panel>
                      </fieldset>
                    )}
                  </Disclosure>
                ))}
              </div>
              <button
                type="button"
                onClick={() => {
                  resetSub();
                  handleReset();
                }}
                className="block w-[90%] mx-auto cursor-pointer place-items-center justify-center rounded border border-gray-300 bg-[#001c72] py-1.5 text-xs font-medium text-gray-700 shadow-sm transition duration-200 ease-linear hover:bg-[#001c72d2] hover:shadow-xl  focus:shadow-lg focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
              >
                <label className="cursor-pointer text-center text-sm  text-white">
                  Reset results
                </label>
              </button>
            </div>
          </Transition.Child>
        </Dialog>
      </Transition.Root>
      {/* DESKTOP FILTER MENU */}
      <aside className="sticky top-5 mb-0 shadow-md pt-2 pb-4 px-4">
        <h2 className="sr-only">Filters</h2>
        <div className="flex w-full place-items-center">
          <p className="mr-auto block text-gray-400 lg:hidden">
            {currentProducts.length} results on this page
          </p>
          <button
            type="button"
            className="inline-flex items-center justify-between justify-self-end rounded-sm border border-[#213F4E] border-opacity-40 py-2 px-5 lg:hidden"
            onClick={() => setMobileFiltersOpen(true)}
          >
            <span className="mr-6 text-sm font-medium text-gray-700">
              Filters
            </span>
            <AdjustmentsIcon
              className="ml-1 h-5 w-5 flex-shrink-0 rotate-90 text-[#213F4E]"
              aria-hidden="true"
            />
          </button>
        </div>

        <div className="hidden divide-y lg:block">
          {/* DESKTOP NAME SEARCH FORM */}
          <form onSubmit={handleSearch}>
            <div className="min-w-0 flex-1 md:px-8 lg:px-0 xl:col-span-6">
              <h2 className="font-bold italic text-[#001c72] underline">
                Search By Name
              </h2>
              <div className="flex items-center px-0 py-5 md:mx-auto md:max-w-3xl lg:mx-0 lg:max-w-none xl:px-0">
                <div className="w-full">
                  <label htmlFor="search" className="sr-only">
                    Search
                  </label>
                  <div className="relative">
                    <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                      <SearchIcon
                        className="h-5 w-5 text-gray-400"
                        aria-hidden="true"
                      />
                    </div>
                    <input
                      id="search"
                      name="search"
                      value={searchValue}
                      onChange={(e) => setSearchValue(e.target.value)}
                      className="block w-full rounded-md border border-gray-300 bg-white py-2 pl-10 pr-3 text-sm placeholder-gray-500 focus:text-gray-900 focus:placeholder-gray-400 focus:shadow-xl focus:outline-none focus:ring-1  sm:text-sm"
                      placeholder="Search"
                      type="search"
                    />
                  </div>
                </div>
              </div>
            </div>
          </form>
          {/* DESKTOP PRICE SEARCH */}
          <form onSubmit={handlePriceSearch}>
            <div className="min-w-0 flex-1 md:px-8 lg:px-0 xl:col-span-6">
              <h2 className="mt-4 text-sm font-medium text-[#001c72]">
                Search By Price
              </h2>
              <div className="flex items-center px-0 py-4 md:mx-auto md:max-w-3xl lg:mx-0 lg:max-w-none xl:px-0">
                <div className="w-full">
                  <label htmlFor="search" className="sr-only">
                    Search
                  </label>
                  <div className="relative">
                    <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                      <CurrencyPoundIcon
                        className="h-5 w-5 text-gray-400"
                        aria-hidden="true"
                      />
                    </div>
                    <input
                      id="search"
                      name="search"
                      value={priceSearchValue}
                      onChange={(e) => setPriceSearchValue(e.target.value)}
                      className="block w-full rounded-md border border-gray-300 bg-white py-2 pl-10 pr-3 text-sm placeholder-gray-500 focus:text-gray-900 focus:placeholder-gray-400 focus:shadow-xl focus:outline-none focus:ring-1  sm:text-sm"
                      placeholder="Search"
                      type="search"
                    />
                  </div>
                </div>
              </div>
            </div>
          </form>
          <div className="space-y-10 divide-y divide-gray-200">
            {filters.map((category, sectionIdx) => (
              <div key={category.name} className="pt-5">
                <fieldset>
                  <legend className="block text-sm font-medium text-[#001c72]">
                    {category.name}
                  </legend>
                  <div
                    className={`space-y-3 pt-6 ${
                      category.name === "Type Of Pet"
                        ? "flex justify-between"
                        : null
                    }`}
                  >
                    {category.options.map(
                      ({ id, value, label, image_src }, optionIdx) =>
                        image_src ? (
                          <button
                            onClick={() => setCatOrDog(() => value)}
                            key={image_src}
                            className="flex items-center"
                          >
                            <div
                              className={`h-20 w-20 xl:h-28 xl:w-28 transform overflow-hidden rounded-full bg-[#fcf1e0] object-cover object-top `}
                            >
                              <img
                                className={`transition duration-300 ease-linear ${
                                  catOrDog === "" && "translate-y-10"
                                } ${
                                  catOrDog === "Dog" && label === "Cat"
                                    ? "translate-y-10 hover:translate-y-0"
                                    : "translate-y-0 hover:translate-y-0"
                                } ${
                                  catOrDog === "Cat" && label === "Dog"
                                    ? "translate-y-10 hover:translate-y-0"
                                    : "translate-y-0 hover:translate-y-0"
                                }`}
                                src={image_src}
                                alt=""
                              />
                            </div>
                          </button>
                        ) : (
                          <button
                            key={id}
                            onClick={() => setSubscription(() => value)}
                            type="button"
                            className="block w-full cursor-pointer place-items-center justify-center rounded border border-gray-300 bg-white px-2.5 py-1.5 text-xs font-medium text-gray-700 shadow-sm transition duration-200 ease-linear hover:bg-gray-50 hover:shadow-md focus:bg-gray-50 focus:shadow-lg focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                          >
                            <label className="cursor-pointer text-center text-sm  text-gray-600">
                              {label}
                            </label>
                          </button>
                        )
                    )}
                  </div>
                </fieldset>
              </div>
            ))}
            <button
              type="button"
              onClick={() => {
                resetSub();
                handleReset();
              }}
              className="block w-full cursor-pointer place-items-center justify-center rounded border border-gray-300 bg-[#001c72] px-2.5 py-1.5 text-xs font-medium text-gray-700 shadow-sm transition duration-200 ease-linear hover:bg-[#001c72d2] hover:shadow-xl  focus:shadow-lg focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
            >
              <label className="cursor-pointer text-center text-sm  text-white">
                Reset results
              </label>
            </button>
          </div>
        </div>
      </aside>
    </aside>
  );
}
