import { createContext, useContext, useState } from "react";

const ProductsContext = createContext({});

export function useProvider() {
  return useContext(ProductsContext);
}
export function ProductsProvider({ children }) {
  const [data, setData] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [pageLimit, setPageLimit] = useState(12);
  const [searchValue, setSearchValue] = useState("");
  const [priceSearchValue, setPriceSearchValue] = useState("");
  const [subscription, setSubscription] = useState(null);
  const [catOrDog, setCatOrDog] = useState("");

  return (
    <ProductsContext.Provider
      value={{
        data,
        setData,
        currentPage,
        setCurrentPage,
        pageLimit,
        setPageLimit,
        searchValue,
        setSearchValue,
        priceSearchValue,
        setPriceSearchValue,
        catOrDog,
        setCatOrDog,
        subscription,
        setSubscription,
      }}
    >
      {children}
    </ProductsContext.Provider>
  );
}
