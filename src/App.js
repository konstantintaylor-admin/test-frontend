import Navbar from "./components/Navbar";
import ProductsCollection from "./components/ProductsCollection";
import { ProductsProvider } from "./contexts/ProductsProvider";
import Footer from "./components/Footer";
import { useEffect, useState } from "react";

function App() {
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 3000);
  }, []);

  return (
    <main>
      <div
        className={`fixed z-[999] ${
          !loading
            ? "transition-all rounded-tl-full rounded-tr-full duration-1000 ease-in translate-y-[100%]"
            : ""
        } w-screen h-screen bg-white flex justify-center place-items-center`}
      >
        <img
          className="w-[50%] animate-bounce object-contain"
          src="https://cdn.builder.io/api/v1/image/assets%2F08d8ed1927034a9791a787768e108aa6%2F18d283cfa2d24f878d6565d828a31025"
          alt=""
        />
      </div>
      <ProductsProvider className="App">
        <Navbar />
        <ProductsCollection />
        <Footer />
      </ProductsProvider>
    </main>
  );
}

export default App;
