# TEST CASES

1. When the user visits the landing page they should be able to select a pet type and subscription preference in the filter sidebar without breaking the get request. Expected results should be 4 results for Cat with subscription yes, and 4 results for Dog with subscription no.

2. Searching by name should return all products with names matching the the letters or words in the search term.

3. When the user clicks on the reset results button all selected filters should be reset.

# SOLUTION

========

# Estimation

Estimated: n hours: 24

Spent: x hours: 17

# Solution

Comments on your solution

I began my usual process for building features by first creating and mapping through some static product data, so that I could create and style ProductsCollection, Sidebar, Navigation and Footer components around it. Once satisfied with the mobile first styles, I made a get request to fetch all of the products from the json-server enpoint and mapped through those results instead of the static data.


SEARCH BY NAME:
The search functionality was set to filter the users search input so that it would match the values of the names of each product. This was done by creating state called searchValue that would pass the search input values to the endpoint url once the user hit the enter key, in turn triggering the handleSearch function to make the request.

SEARCH BY PRICE:
The logic for searching by price was very similar to searching by name, only that the query and values passed in the json-server were a little different. To get the desired effect of showing the user the closest price to their search, I began by starting the json-server url with _sort=price and _order=desc operators, followed by the _lte (less that or equal to) operator to meet the project requirements of showing one product when searching 30. I felt that this approach would be great in a real life scenario, as it could potentially result in a customer buying more products than they had originally planned too, due to not only seeing the exact value that they searched for.

FILTER BY PET & SUBSCRIPTION:
Creating the logic to meet the requirements for filtering by subscription and pet type was what took me the longest out of the three. I kept running into issues where I could not access the values in the tags array of each json object while using the usual filtering methods. The request would also fail if either the pet type or subscription options were not selected by the user.

To overcome this, I had to find a way to make a get request with both filters for tags and subscription, without breaking the request. After reading the json-server documentation, I found that that there was an operator that would allow me to access the tags by appending the _like operator to the tags part of the url. With this, I was able to solve the first issue. To resolve the last problem I simply created a function that would check to see if my subscription state was set to null, and if it was then no filter query for subscription would be rendered in the template literal that was being used in the get request url, and only the pet type would be requested.

To consolidate the logic I decided to create one master function with switch cases for searchTitle, searchPrice and filter so that I could reset state values, and the data that was being mapped, depending on which function was being triggered by the user.


HOW THE APP COULD BE IMPROVED:
In terms of performance and scalability it could be nice to migrate the project to Next Js, due to it's server side rendering capabilities that would reduce build times quite significantly once the app was scaled up. Next Js also comes with an Image component that would render the most appropriate format and size for each image, depending on the device being used. This would in turn save time in having to code this going forwards if performance was an issue due to image size.

To improve the current apps functionality, perhaps it could be nice to give the user even more flexibility on their search ability by implementing a sort functionality to sort based on each of the available. In terms of how the filter parameters work together, the app could also be improved by merging all of the filter options so that the search is more specific.

Lastly I feel that there is something quite fun and playful about pets as a whole, and I think if there was more time it would be great to translate this in the app with more interactive animations depending on if the company was open to going down that route and matched their brand vision.